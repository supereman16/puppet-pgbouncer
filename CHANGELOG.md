## 2015-03-04 Release 0.1.2
### Summary
Account Migration

### Changed
 - Migrated from github to bitbucket
 - Changed ownership of puppetforge account
 - Change log migrated to markdown.

## 2015-02-20 Version 0.1.1
### Summary
Added additional parameter

### Changes
 - Added openhub badge to readme.
 - Added pool_mode
 - Added auth_type

## 2015-02-19 Version 0.1.0
### Summary
Initial Release.
